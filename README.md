# `mediawiki-export`

[![](https://shields.kaki87.net/npm/v/mediawiki-export)](https://www.npmjs.com/package/mediawiki-export)
![](https://shields.kaki87.net/npm/dw/mediawiki-export)
![](https://shields.kaki87.net/npm/l/mediawiki-export)

MediaWiki export from sitemap to HTML.

## Getting started

- Install with `yarn global add mediawiki-export` or `npm i -g mediawiki-export`
- Run `mediawiki-export <sitemap>` with `<sitemap>` being a URL to a `sitemapindex` or a `urlset`.

Default file extension is `html`, an `--extension` option can be added to change it, e.g. `--extension md`.

![](https://git.kaki87.net/KaKi87/mediawiki-export/media/branch/master/screenshot.gif)